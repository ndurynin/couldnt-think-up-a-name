#include <stdio.h>
#include <stdlib.h>

void print_array(int * a, int size)
{
    int i;
    for(i = 0; i < size; i++)
        printf("%d ", a[i]);
    printf("\n");
}

int compare(const void * a, const void * b)
{
    return *(int*)a - *(int*)b;
}

// a - a pointer to the beginning of the array
// size - the size of the array
// x - x < a[result] if such a[result] exists
// returns result - a position of the result (-1 is such result doesn't exist)
int my_bsearch(int * a, int size, int x)
{
    int left = -1;
    int right = size;
    while (right - left > 1)
    {
        int middle = (left + right) >> 1;
        if (x < a[middle])
            right = middle;
        else
            left = middle;
    }
    if (right == size)
        return -1;
    return right;
}

int main()
{
    printf("Array size: ");
    int size;
    scanf("%d", &size);
    int * a = (int*)malloc(size * sizeof(int));
    int i;
    for(i = 0; i < size; i++)
        a[i] = rand() % 10;
    printf("Array:\n");
    print_array(a, size);
    qsort(a, size, sizeof(int), compare);
    printf("Sorted:\n");
    print_array(a, size);
    for(i = -1; i <= 10; i++)
        printf("%d -> %d\n", i, my_bsearch(a, size, i)); 
    return 0;
}
